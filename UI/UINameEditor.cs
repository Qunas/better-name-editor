using System;
using log4net;
using log4net.Core;
using log4net.Repository.Hierarchy;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Mono.Cecil.Cil;
using MonoMod.Cil;
using ReLogic.Content;
using ReLogic.OS;
using Terraria;
using BetterNameEditor;
using Terraria.Audio;
using Terraria.GameContent.UI.Elements;
using Terraria.GameContent.UI.States;
using Terraria.GameInput;
using Terraria.Initializers;
using Terraria.Localization;
using Terraria.ModLoader;
using Terraria.UI;
using System.Globalization;
using Terraria.GameContent;
using System.Linq;
using System.Drawing.Printing;
using Terraria.UI.Chat;

namespace BetterNameEditor.UI
{
    public class UINameEditor : UIElement
    {
        private enum HSLSliderId
        {
            Hue,
            Saturation,
            Luminance
        }
        private UIColoredImageButton _colorButton;

        private UIColoredImageButton _itemButton;

        private UIColoredImageButton _glyphButton;

        private UIColoredImageButton _confirmColorButton;

        private UIColoredImageButton _clearColorButton;

        private UIImageButton _tipButton;

        private UIPanel _editorPanel;

        private UIElement _categoryContainer;

        private UIElement _mainContainer;

        private UIElement _parentLayer;

        private UIVirtualKeyboard _parentVirtualKeyboard;

        private UIColoredImageButton _copyHexButton;

        private UIColoredImageButton _pasteHexButton;

        private UIColoredImageButton _randomColorButton;

        private UIElement _hslContainer;

        private UIElement _itemContainer;

        private UIElement _glyphContainer;

        private UIText _hslHexText;

        private UIText _charCounter;

        private Vector3 _currentColorHSL = RgbToScaledHsl(Color.White);

        private bool _applyColor = false;

        public UINameEditor(UIElement parentLayer, UIVirtualKeyboard parentVirtualKeyboard)
        {
            _parentLayer = parentLayer;
            _parentVirtualKeyboard = parentVirtualKeyboard;
            BuildEditor();
        }

        private void BuildEditor()
        {
            _editorPanel = new UIPanel
            {
                VAlign = 0.18f,
                Width = StyleDimension.FromPercent(1f),
                Height = StyleDimension.FromPixels(290f),
                BackgroundColor = new Color(33, 43, 79) * 0.8f
            };
            _editorPanel.SetPadding(0f);
            _parentLayer.Append(_editorPanel);

            MakeCategoryContainer();
            MakeMainContainer();

            MakeColorMenu(_mainContainer);
            MakeItemMenu(_mainContainer);
            MakeGlyphMenu(_mainContainer);


            Click_Color(null, null);
            Click_ClearColor(null, null);
            Click_RandomColor(null, null);
        }

        private void MakeGlyphMenu(UIElement parentContainer)
        {
            _glyphContainer = new UIElement
            {
                Width = StyleDimension.FromPixelsAndPercent(0f, 1f),
                Height = StyleDimension.FromPixelsAndPercent(0f, 1f),
                HAlign = 0.5f,
                VAlign = 0.5f
            };
            UIGlyphGrid glyphGrid = new()
            {
                Width = StyleDimension.FromPercent(1f),
                Height = StyleDimension.FromPercent(1f)
            };
            glyphGrid.OnGlyphPressed += Click_GlyphButton;
            _glyphContainer.Append(glyphGrid);
            parentContainer.Append(_glyphContainer);
        }

        private void Click_GlyphButton(string glyph)
        {
            _parentVirtualKeyboard.Text += glyph;
        }

        private void MakeItemMenu(UIElement parentContainer)
        {
            _itemContainer = new UIElement
            {
                Width = StyleDimension.FromPixelsAndPercent(0f, 0.5f),
                Height = StyleDimension.FromPixelsAndPercent(0f, 0.5f),
                HAlign = 0.5f,
                VAlign = 0.5f
            };
            UIText itemText = new UIText("This is item menu")
            {
                HAlign = 0.5f,
                VAlign = 0.5f
            };
            _itemContainer.Append(itemText);
            parentContainer.Append(_itemContainer);
        }

        private void MakeCategoryContainer()
        {
            _categoryContainer = new UIElement
            {
                Width = StyleDimension.FromPixelsAndPercent(0f, 1f),
                Height = StyleDimension.FromPixelsAndPercent(50f, 0f)
            };
            _categoryContainer.SetPadding(0f);
            _categoryContainer.PaddingTop = 4f;
            _categoryContainer.PaddingBottom = 0f;
            _editorPanel.Append(_categoryContainer);

            _colorButton = new(ModContent.Request<Texture2D>("BetterNameEditor/Assets/Textures/ColorSelect"))
            {
                HAlign = 0.40f,
                VAlign = -0.05f,
                Width = StyleDimension.FromPixels(44f),
                Height = StyleDimension.FromPixels(44f)
            };
            _colorButton.OnLeftMouseDown += Click_Color;
            _categoryContainer.Append(_colorButton);

            _itemButton = new(ModContent.Request<Texture2D>("BetterNameEditor/Assets/Textures/ItemSelect"))
            {
                HAlign = 0.50f,
                VAlign = -0.05f,
                Width = StyleDimension.FromPixels(44f),
                Height = StyleDimension.FromPixels(44f)
            };
            _itemButton.OnLeftMouseDown += Click_Item;
            _categoryContainer.Append(_itemButton);

            _glyphButton = new(ModContent.Request<Texture2D>("BetterNameEditor/Assets/Textures/GlyphSelect"))
            {
                HAlign = 0.60f,
                VAlign = -0.05f,
                Width = StyleDimension.FromPixels(44f),
                Height = StyleDimension.FromPixels(44f)
            };
            _glyphButton.OnLeftMouseDown += Click_Glyph;
            _categoryContainer.Append(_glyphButton);
            UIPanel uIPanel = new UIPanel()
            {
                HAlign = 0.98f,
                VAlign = 0.5f,
                Width = StyleDimension.FromPixelsAndPercent(80f, 0f),
                Height = StyleDimension.FromPixelsAndPercent(32f, 0f)
            };
            _charCounter = new UIText("0/200")
            {
                HAlign = 0.5f,
                VAlign = 0.5f
            };
            uIPanel.Append(_charCounter);
            _categoryContainer.Append(uIPanel);
            UIHorizontalSeparator horizontalSeparator = new UIHorizontalSeparator
            {
                Width = StyleDimension.FromPixelsAndPercent(-20f, 1f),
                Top = StyleDimension.FromPixels(6f),
                VAlign = 1f,
                HAlign = 0.5f,
                Color = Color.Lerp(Color.White, new Color(63, 65, 151, 255), 0.85f) * 0.9f
            };
            _categoryContainer.Append(horizontalSeparator);

        }

        private void MakeMainContainer()
        {
            _mainContainer = new UIElement
            {
                Top = StyleDimension.FromPixelsAndPercent(_categoryContainer.Height.Pixels + 6f, 0f),
                Width = StyleDimension.FromPixelsAndPercent(0f, 1f),
                Height = StyleDimension.FromPixelsAndPercent(_editorPanel.Height.Pixels - 70f, 0f)
            };
            _editorPanel.Append(_mainContainer);
        }

        private void MakeColorMenu(UIElement parentContainer)
        {
            _hslContainer = new UIElement
            {
                Width = StyleDimension.FromPixelsAndPercent(220f, 0f),
                Height = StyleDimension.FromPixelsAndPercent(214f, 0f),
                HAlign = 0.5f,
                VAlign = 0.5f
            };
            _hslContainer.SetPadding(0f);
            parentContainer.Append(_hslContainer);
            UIElement uIElement2 = new UIPanel
            {
                Width = StyleDimension.FromPixelsAndPercent(220f, 0f),
                Height = StyleDimension.FromPixelsAndPercent(104f, 0f),
                HAlign = 0.5f,
                VAlign = 0f,
                Top = StyleDimension.FromPixelsAndPercent(10f, 0f)
            };
            uIElement2.SetPadding(0f);
            uIElement2.PaddingTop = 3f;
            _hslContainer.Append(uIElement2);
            uIElement2.Append(CreateHSLSlider(HSLSliderId.Hue));
            uIElement2.Append(CreateHSLSlider(HSLSliderId.Saturation));
            uIElement2.Append(CreateHSLSlider(HSLSliderId.Luminance));
            UIPanel uIPanel = new UIPanel
            {
                HAlign = 1f,
                Top = StyleDimension.FromPixelsAndPercent(uIElement2.Height.Pixels + uIElement2.Top.Pixels + 12f, 0f),
                Width = StyleDimension.FromPixelsAndPercent(100f, 0f),
                Height = StyleDimension.FromPixelsAndPercent(32f, 0f)
            };
            _hslHexText = new UIText("#FFFFFF")
            {
                VAlign = 0.5f,
                HAlign = 0.5f
            };
            uIPanel.Append(_hslHexText);
            _hslContainer.Append(uIPanel);
            _copyHexButton = new UIColoredImageButton(Main.Assets.Request<Texture2D>("Images/UI/CharCreation/Copy", (AssetRequestMode)1), isSmall: true)
            {
                HAlign = 0f,
                Top = StyleDimension.FromPixelsAndPercent(uIElement2.Height.Pixels + uIElement2.Top.Pixels + 12f, 0f),
                Left = StyleDimension.FromPixelsAndPercent(0f, 0f)
            };
            _copyHexButton.OnLeftClick += Click_CopyHex;
            _hslContainer.Append(_copyHexButton);
            _pasteHexButton = new UIColoredImageButton(Main.Assets.Request<Texture2D>("Images/UI/CharCreation/Paste", (AssetRequestMode)1), isSmall: true)
            {
                HAlign = 0f,
                Top = StyleDimension.FromPixelsAndPercent(uIElement2.Height.Pixels + uIElement2.Top.Pixels + 12f, 0f),
                Left = StyleDimension.FromPixelsAndPercent(40f, 0f)
            };
            _pasteHexButton.OnLeftClick += Click_PasteHex;
            _hslContainer.Append(_pasteHexButton);
            _randomColorButton = new UIColoredImageButton(Main.Assets.Request<Texture2D>("Images/UI/CharCreation/Randomize", (AssetRequestMode)1), isSmall: true)
            {
                HAlign = 0f,
                Top = StyleDimension.FromPixelsAndPercent(uIElement2.Height.Pixels + uIElement2.Top.Pixels + 12f, 0f),
                Left = StyleDimension.FromPixelsAndPercent(80f, 0f)
            };
            _randomColorButton.OnLeftClick += Click_RandomColor;
            _hslContainer.Append(_randomColorButton);
            _copyHexButton.SetSnapPoint("Low", 0);
            _pasteHexButton.SetSnapPoint("Low", 1);
            _randomColorButton.SetSnapPoint("Low", 2);

            _confirmColorButton = new(ModContent.Request<Texture2D>("BetterNameEditor/Assets/Textures/Confirm"))
            {
                VAlign = 1f,
                HAlign = 0.3f,
                Width = StyleDimension.FromPixels(44f),
                Height = StyleDimension.FromPixels(44f)
            };
            _confirmColorButton.OnLeftClick += Click_ConfirmColor;
            _hslContainer.Append(_confirmColorButton);
            _clearColorButton = new(ModContent.Request<Texture2D>("BetterNameEditor/Assets/Textures/Cancel"))
            {
                VAlign = 1f,
                HAlign = 0.7f,
                Width = StyleDimension.FromPixels(44f),
                Height = StyleDimension.FromPixels(44f)
            };
            _clearColorButton.OnLeftClick += Click_ClearColor;
            _hslContainer.Append(_clearColorButton);
            _tipButton = new(ModContent.Request<Texture2D>("BetterNameEditor/Assets/Textures/TipIcon"))            
            {
                VAlign = 0.953f,
                HAlign = 0.94f,
                Width = StyleDimension.FromPixels(30f),
                Height = StyleDimension.FromPixels(30f)
            };
            _tipButton.SetHoverImage(ModContent.Request<Texture2D>("BetterNameEditor/Assets/Textures/TipIcon_MouseOver"));
            _tipButton.SetVisibility(1f,1f);
            _hslContainer.Append(_tipButton);

        }

        private void Click_ConfirmColor(UIMouseEvent evt, UIElement listeningElement)
        {
            _clearColorButton.SetSelected(false);
            _confirmColorButton.SetSelected(true);
            _applyColor = true;
            Color color = ScaledHslToRgb(_currentColorHSL.X, _currentColorHSL.Y, _currentColorHSL.Z);
            ApplyColorTag(color);
        }

        private void Click_ClearColor(UIMouseEvent evt, UIElement listeningElement)
        {
            _confirmColorButton.SetSelected(false);
            _clearColorButton.SetSelected(true);
            _applyColor = false;
            ClearColorTags();
        }

        private void Click_RandomColor(UIMouseEvent evt, UIElement listeningElement)
        {
            Vector3 randomColorVector = GetRandomColorVector();
            ApplyColorTag(ScaledHslToRgb(randomColorVector.X, randomColorVector.Y, randomColorVector.Z));
            _currentColorHSL = randomColorVector;
            UpdateHexText(ScaledHslToRgb(randomColorVector.X, randomColorVector.Y, randomColorVector.Z));
        }

        private static Vector3 GetRandomColorVector()
        {
            return new Vector3(Main.rand.NextFloat(), Main.rand.NextFloat(), Main.rand.NextFloat());
        }

        private void Click_PasteHex(UIMouseEvent evt, UIElement listeningElement)
        {
            string value = Platform.Get<IClipboard>().Value;
            if (GetHexColor(value, out var hsl))
            {
                ApplyColorTag(ScaledHslToRgb(hsl.X, hsl.Y, hsl.Z));
                _currentColorHSL = hsl;
                UpdateHexText(ScaledHslToRgb(hsl.X, hsl.Y, hsl.Z));
            }
        }

        private void Click_CopyHex(UIMouseEvent evt, UIElement listeningElement)
        {
            Platform.Get<IClipboard>().Value = _hslHexText.Text;
        }

        private bool GetHexColor(string hexString, out Vector3 hsl)
        {
            if (hexString.StartsWith("#"))
            {
                hexString = hexString.Substring(1);
            }
            if (hexString.Length <= 6 && uint.TryParse(hexString, NumberStyles.HexNumber, CultureInfo.CurrentCulture, out var result))
            {
                uint b = result & 0xFFu;
                uint g = (result >> 8) & 0xFFu;
                uint r = (result >> 16) & 0xFFu;
                hsl = RgbToScaledHsl(new Color((int)r, (int)g, (int)b));
                return true;
            }
            hsl = Vector3.Zero;
            return false;
        }

        private UIColoredSlider CreateHSLSlider(HSLSliderId id)
        {
            UIColoredSlider uIColoredSlider = CreateHSLSliderButtonBase(id);
            uIColoredSlider.VAlign = 0f;
            uIColoredSlider.HAlign = 0f;
            uIColoredSlider.Width = StyleDimension.FromPixelsAndPercent(-10f, 1f);
            uIColoredSlider.Top.Set(30 * (int)id, 0f);
            //uIColoredSlider.OnLeftMouseDown += Click_ColorPicker;
            uIColoredSlider.SetSnapPoint("Middle", (int)id, null, new Vector2(0f, 20f));
            return uIColoredSlider;
        }

        private UIColoredSlider CreateHSLSliderButtonBase(HSLSliderId id)
        {
            return id switch
            {
                HSLSliderId.Saturation => new UIColoredSlider(LocalizedText.Empty, () => GetHSLSliderPosition(HSLSliderId.Saturation), delegate (float x)
                {
                    UpdateHSLValue(HSLSliderId.Saturation, x);
                }, UpdateHSL_S, (float x) => GetHSLSliderColorAt(HSLSliderId.Saturation, x), Color.Transparent),
                HSLSliderId.Luminance => new UIColoredSlider(LocalizedText.Empty, () => GetHSLSliderPosition(HSLSliderId.Luminance), delegate (float x)
                {
                    UpdateHSLValue(HSLSliderId.Luminance, x);
                }, UpdateHSL_L, (float x) => GetHSLSliderColorAt(HSLSliderId.Luminance, x), Color.Transparent),
                _ => new UIColoredSlider(LocalizedText.Empty, () => GetHSLSliderPosition(HSLSliderId.Hue), delegate (float x)
                {
                    UpdateHSLValue(HSLSliderId.Hue, x);
                }, UpdateHSL_H, (float x) => GetHSLSliderColorAt(HSLSliderId.Hue, x), Color.Transparent),
            };
        }
        private float GetHSLSliderPosition(HSLSliderId id)
        {
            return id switch
            {
                HSLSliderId.Hue => _currentColorHSL.X,
                HSLSliderId.Saturation => _currentColorHSL.Y,
                HSLSliderId.Luminance => _currentColorHSL.Z,
                _ => 1f,
            };
        }

        private void UpdateHSL_H()
        {
            float value = UILinksInitializer.HandleSliderHorizontalInput(_currentColorHSL.X, 0f, 1f, PlayerInput.CurrentProfile.InterfaceDeadzoneX, 0.35f);
            UpdateHSLValue(HSLSliderId.Hue, value);
        }

        private void UpdateHSL_S()
        {
            float value = UILinksInitializer.HandleSliderHorizontalInput(_currentColorHSL.Y, 0f, 1f, PlayerInput.CurrentProfile.InterfaceDeadzoneX, 0.35f);
            UpdateHSLValue(HSLSliderId.Saturation, value);
        }

        private void UpdateHSL_L()
        {
            float value = UILinksInitializer.HandleSliderHorizontalInput(_currentColorHSL.Z, 0f, 1f, PlayerInput.CurrentProfile.InterfaceDeadzoneX, 0.35f);
            UpdateHSLValue(HSLSliderId.Luminance, value);
        }

        private void UpdateHSLValue(HSLSliderId id, float value)
        {
            switch (id)
            {
                case HSLSliderId.Hue:
                    _currentColorHSL.X = value;
                    break;
                case HSLSliderId.Saturation:
                    _currentColorHSL.Y = value;
                    break;
                case HSLSliderId.Luminance:
                    _currentColorHSL.Z = value;
                    break;
            }
            Color color = ScaledHslToRgb(_currentColorHSL.X, _currentColorHSL.Y, _currentColorHSL.Z);

            ApplyColorTag(color);


            UpdateHexText(color);
        }

        private void ApplyColorTag(Color color)
        {
            if (!_applyColor) return;

            string text = _parentVirtualKeyboard.Text;

            string[] splitText = text.Split(new char[] { '[', ']' });

            int lastColorTag = 0, lastItemTag = 0;

            for (int i = 0; i < splitText.Length; i++)
            {
                lastItemTag++;
                lastColorTag++;
                if (String.IsNullOrWhiteSpace(splitText[i]))
                {
                    continue;
                }
                if (splitText[i].StartsWith("i:") || splitText[i].StartsWith("g:") || splitText[i].StartsWith("a:") || splitText[i].StartsWith("n:"))
                {
                    lastItemTag = 0;
                    continue;
                }
                if (splitText[i].StartsWith("c/") && splitText[i].Length >= 9)
                {
                    if (lastColorTag >= lastItemTag)
                    {
                        splitText[i] = splitText[i].Remove(2, 6);
                        splitText[i] = splitText[i].Insert(2, GetHex(color));
                        lastColorTag = 0;
                    }
                    else
                    {
                        splitText[i] = splitText[i].Remove(0, 9);
                        splitText[i - lastColorTag] = splitText[i - lastColorTag] + splitText[i];
                        splitText[i] = "";
                    }
                }
                else
                {
                    if (lastColorTag >= lastItemTag)
                    {
                        splitText[i] = "c/" + GetHex(color) + ":" + splitText[i];
                        lastColorTag = 0;
                    }
                    else
                    {
                        splitText[i - lastColorTag] = splitText[i - lastColorTag] + splitText[i];
                        splitText[i] = "";
                    }
                }
            }
            for (int i = 0; i < splitText.Length; i++)
            {
                if (String.IsNullOrWhiteSpace(splitText[i])) continue;

                splitText[i] = "[" + splitText[i] + "]";
            }
            text = String.Join("", splitText);
            _parentVirtualKeyboard.Text = text;
        }

        private void ClearColorTags()
        {
            string text = _parentVirtualKeyboard.Text;

            if (String.IsNullOrEmpty(text)) return;

            string[] splitText = text.Split(new char[] { '[', ']' });
            for (int i = 0; i < splitText.Length; i++)
            {
                if (splitText[i].StartsWith("c/") && splitText[i].Length >= 9)
                {
                    splitText[i] = splitText[i].Remove(0, 9);
                }
            }
            for (int i = 0; i < splitText.Length; i++)
            {
                if (String.IsNullOrWhiteSpace(splitText[i])) continue;

                if (splitText[i].StartsWith("i:") || splitText[i].StartsWith("g:") || splitText[i].StartsWith("a:") || splitText[i].StartsWith("n:")) splitText[i] = "[" + splitText[i] + "]";
            }
            text = String.Join("", splitText);
            _parentVirtualKeyboard.Text = text;
        }

        private Color GetHSLSliderColorAt(HSLSliderId id, float pointAt)
        {
            return id switch
            {
                HSLSliderId.Hue => ScaledHslToRgb(pointAt, 1f, 0.5f),
                HSLSliderId.Saturation => ScaledHslToRgb(_currentColorHSL.X, pointAt, _currentColorHSL.Z),
                HSLSliderId.Luminance => ScaledHslToRgb(_currentColorHSL.X, _currentColorHSL.Y, pointAt),
                _ => Color.White,
            };
        }

        private void UpdateHexText(Color pendingColor)
        {
            _hslHexText.SetText(GetHexText(pendingColor));
        }

        private static string GetHexText(Color pendingColor)
        {
            return "#" + pendingColor.Hex3().ToUpper();
        }

        private static string GetHex(Color pendingColor)
        {
            return pendingColor.Hex3().ToUpper();
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            UpdateCharCounter();
            string text = null;
            if (_copyHexButton.IsMouseHovering)
            {
                text = Language.GetTextValue("UI.CopyColorToClipboard");
            }
            if (_pasteHexButton.IsMouseHovering)
            {
                text = Language.GetTextValue("UI.PasteColorFromClipboard");
            }
            if (_randomColorButton.IsMouseHovering)
            {
                text = Language.GetTextValue("UI.RandomizeColor");
            }
            if (_confirmColorButton.IsMouseHovering)
            {
                text = Language.GetTextValue("Mods.BetterNameEditor.UI.ApplyColor");
            }
            if (_clearColorButton.IsMouseHovering)
            {
                text = Language.GetTextValue("Mods.BetterNameEditor.UI.RemoveColor");
            }
            if (_tipButton.IsMouseHovering)
            {
                text = Language.GetTextValue("Mods.BetterNameEditor.UI.ColorTagTip");
            }
            if (_charCounter.Parent.IsMouseHovering)
            {
                text = Language.GetTextValue("Mods.BetterNameEditor.UI.CharLimitTip");;
            }

            if (text != null)
            {
                float x = ChatManager.GetStringSize(FontAssets.MouseText.Value ,text, Vector2.One).X;
                Vector2 vector = new Vector2(Main.mouseX, Main.mouseY) + new Vector2(16f);
                if (vector.Y > (float)(Main.screenHeight - 30))
                {
                    vector.Y = Main.screenHeight - 30;
                }
                if (vector.X > (float)Main.screenWidth - x)
                {
                    vector.X = Main.screenWidth - 460;
                }
                TweakedUtils.DrawBorderStringFourWay(spriteBatch, FontAssets.MouseText.Value, text, vector.X, vector.Y, new Color(Main.mouseTextColor, Main.mouseTextColor, Main.mouseTextColor, Main.mouseTextColor), Color.Black, Vector2.Zero);
            }
        }

        public void UpdateCharCounter()
        {
            if (_parentVirtualKeyboard.Text.Length >= BetterNameEditor.charLimit)
            {
                _charCounter.SetText("[c/f74343:" + _parentVirtualKeyboard.Text.Length + "/" + BetterNameEditor.charLimit + "]");
            }
            else if (_parentVirtualKeyboard.Text.Length > 20 && _parentVirtualKeyboard.Text.Length < BetterNameEditor.charLimit)
            {
                _charCounter.SetText("[c/f7be43:" + _parentVirtualKeyboard.Text.Length + "/" + BetterNameEditor.charLimit + "]");
            }
            else {
                _charCounter.SetText(_parentVirtualKeyboard.Text.Length + "/" + BetterNameEditor.charLimit);
            }
        }
        private void UnselectAll()
        {
            _colorButton.SetSelected(false);
            _itemButton.SetSelected(false);
            _glyphButton.SetSelected(false);
            _hslContainer.Remove();
            _itemContainer.Remove();
            _glyphContainer.Remove();
        }

        private void Click_Color(UIMouseEvent evt, UIElement listeningElement)
        {
            UnselectAll();
            _colorButton.SetSelected(true);
            _mainContainer.Append(_hslContainer);
        }

        private void Click_Item(UIMouseEvent evt, UIElement listeningElement)
        {
            UnselectAll();
            _itemButton.SetSelected(true);
            _mainContainer.Append(_itemContainer);
        }

        private void Click_Glyph(UIMouseEvent evt, UIElement listeningElement)
        {
            UnselectAll();
            _glyphButton.SetSelected(true);
            _mainContainer.Append(_glyphContainer);
        }
        private static Color ScaledHslToRgb(float hue, float saturation, float luminosity)
        {
            return Main.hslToRgb(hue, saturation, luminosity * 0.85f + 0.15f);
        }
        private static Vector3 RgbToScaledHsl(Color color)
        {
            Vector3 value = Main.rgbToHsl(color);
            value.Z = (value.Z - 0.15f) / 0.85f;
            return Vector3.Clamp(value, Vector3.Zero, Vector3.One);
        }

    }

}