using System;
using log4net;
using log4net.Core;
using log4net.Repository.Hierarchy;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Mono.Cecil.Cil;
using MonoMod.Cil;
using ReLogic.Content;
using ReLogic.OS;
using Terraria;
using BetterNameEditor;
using Terraria.Audio;
using Terraria.GameContent.UI.Elements;
using Terraria.GameContent.UI.States;
using Terraria.GameInput;
using Terraria.Initializers;
using Terraria.Localization;
using Terraria.ModLoader;
using Terraria.UI;
using System.Globalization;
using Terraria.GameContent;
using System.Linq;

namespace BetterNameEditor.UI
{
    public class UIGlyphGrid : UIElement
    {
        public event Action<string> OnGlyphPressed;
        public UIGlyphGrid()
        {
            UIPanel glyphList = new()
            {
                Width = StyleDimension.FromPixelsAndPercent(14f, 0.7f),
                Height = StyleDimension.FromPixelsAndPercent(6f, 0.9f),
                VAlign = 0.5f,
                HAlign = 0.5f
            };
            glyphList.SetPadding(0f);
            Append(glyphList);

            for (int i = 0; i < 26; i++)
            {
                UIGlyphButton glyph = new(i)
                {
                    Width = StyleDimension.FromPixels(44f),
                    Height = StyleDimension.FromPixels(44f),
                    Left = StyleDimension.FromPixels((float)(i % 8) * 47f + 9f),
                    Top = StyleDimension.FromPixels((float) (i / 8) * 47f + 9f)
                };
                glyphList.Append(glyph);
                glyph.OnLeftClick += Click_GlyphButton;
            }
        }

        private void Click_GlyphButton(UIMouseEvent evt, UIElement listeningElement)
        {
            OnGlyphPressed.Invoke(((UIGlyphButton)listeningElement).glyphText);
        }
    }
}