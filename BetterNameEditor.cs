using System;
using Humanizer;
using log4net.Repository.Hierarchy;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Mono.Cecil.Cil;
using MonoMod.Cil;
using BetterNameEditor.UI;
using Terraria;
using Terraria.GameContent;
using Terraria.GameContent.UI.Elements;
using Terraria.GameContent.UI.States;
using Terraria.ModLoader;
using Terraria.UI;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Reflection;
using Terraria.Chat;
using Terraria.GameContent.UI.Chat;
using Mono.Cecil;
using Terraria.GameContent.ObjectInteractions;
using Terraria.GameContent.UI;
using Terraria.UI.Chat;
using ReLogic.Content;
using ReLogic.Graphics;
using Terraria.Utilities;

namespace BetterNameEditor
{
    public class BetterNameEditor : Mod
    {
        // Going higher than 200 is danger zone and throws PathTooLongException when trying to create a save file
        // TODO: trim file name if it's too long
        public static int charLimit = 200;

        public override void Load()
        {
            Player.nameLen = Int32.MaxValue;
            if (!Main.dedServ)
            {
                IL_UICharacterCreation.Click_Naming += IL_UICharacterCreation_Click_Naming;
                IL_UICharacterCreation.Click_NamingAndCreating += IL_UICharacterCreation_Click_Naming;
                IL_UICharacterListItem.RenameButtonClick += IL_UICharacterCreation_Click_Naming;
                IL_UIWorldCreation.Click_SetName += IL_UIWorldCreation_Click_Naming;
                IL_UIWorldCreation.Click_NamingAndCreating += IL_UIWorldCreation_Click_Naming;
                IL_UIWorldListItem.RenameButtonClick += IL_UIWorldCreation_Click_Naming;
                IL_UIVirtualKeyboard.ctor += IL_UIVirtualKeyboard_ctor;
                On_UIVirtualKeyboard.ctor += On_UIVirtualKeyboard_ctor;

                IL_Main.DrawMouseOver += IL_Main_DrawMouseOver;
                IL_ChatHelper.DisplayMessage += IL_ChatHelper_DisplayMessage;
                IL_NewMultiplayerClosePlayersOverlay.PlayerOnScreenCache.DrawPlayerName_WhenPlayerIsOnScreen += IL_DrawPlayerName_WhenPlayerIsOnScreen;
                IL_NewMultiplayerClosePlayersOverlay.PlayerOffScreenCache.DrawPlayerName += IL_DrawPlayerName_WhenPlayerIsOffScreen;
            }
        }

        private void IL_Main_DrawMouseOver(ILContext il)
        {
            var c = new ILCursor(il);
            c.GotoNext(i => i.MatchLdcI4(460));
            c.GotoNext(i => i.MatchLdsfld(typeof(FontAssets).GetField(nameof(FontAssets.MouseText))));
            c.GotoNext(i => i.MatchCallvirt(typeof(DynamicSpriteFont).GetMethod(nameof(DynamicSpriteFont.MeasureString))));
            c.Remove();
            c.EmitDelegate<Func<DynamicSpriteFont, string, Vector2>>((font, text) =>
            {
                return ChatManager.GetStringSize(font, text, Vector2.One, -1);
            });

            c.GotoNext(i => i.MatchCall(typeof(Utils).GetMethod(nameof(Utils.DrawBorderStringFourWay))));
            c.Remove();
            c.Emit(OpCodes.Call, typeof(TweakedUtils).GetMethod(nameof(TweakedUtils.DrawBorderStringFourWay)));
        }

        private void IL_DrawPlayerName_WhenPlayerIsOnScreen(ILContext il)
        {
            try
            {
                var c = new ILCursor(il);
                var p = typeof(NewMultiplayerClosePlayersOverlay).GetNestedType("PlayerOnScreenCache", BindingFlags.NonPublic);
                var newMpClosePlayersOverlayType = typeof(NewMultiplayerClosePlayersOverlay);
                c.GotoNext(i => i.MatchLdarg1());


                //~~ _pos = _pos + FontAssets.MouseText.Value.MeasureString(_name) - ChatManager.GetStringSize(FontAssets.MouseText.Value, _name, Vector2.One, -1f) / 2;
                   
                c.Emit(OpCodes.Ldarg_0);

                c.Emit(OpCodes.Ldarg_0);
                c.EmitLdfld(p.GetField("_name", BindingFlags.NonPublic | BindingFlags.Instance));
                c.Emit(OpCodes.Ldarg_0);
                c.EmitLdfld(p.GetField("_pos", BindingFlags.NonPublic | BindingFlags.Instance));
                
                c.EmitDelegate<Func<string, Vector2, Vector2>>((name, pos) => {
                    return pos + Vector2.Divide(FontAssets.MouseText.Value.MeasureString(name) - ChatManager.GetStringSize(FontAssets.MouseText.Value, name, Vector2.One, -1f), 2);
                });

                c.EmitStfld(p.GetField("_pos", BindingFlags.NonPublic | BindingFlags.Instance));
                

                //~~ ChatManager.DrawColorCodedStringWithShadow(spriteBatch, FontAssets.MouseText.Value, _name, _pos + loc_1, _color, 0f, Vector2.Zero, Vector2.One, -1f, 2f);

                c.Emit(OpCodes.Ldarg_1);
                c.Emit(OpCodes.Ldarg_0);
                c.EmitLdfld(p.GetField("_name", BindingFlags.NonPublic | BindingFlags.Instance));
                c.Emit(OpCodes.Ldarg_0);
                c.EmitLdfld(p.GetField("_pos", BindingFlags.NonPublic | BindingFlags.Instance));
                c.Emit(OpCodes.Ldarg_0);
                c.EmitLdfld(p.GetField("_color", BindingFlags.NonPublic | BindingFlags.Instance));

                c.EmitDelegate<Func<SpriteBatch, string, Vector2, Color, Vector2>>((spriteBatch, name, pos, color) =>{
                    return ChatManager.DrawColorCodedStringWithShadow(spriteBatch, FontAssets.MouseText.Value, name, pos, color, 0f, Vector2.Zero, Vector2.One);
                });

                c.Emit(OpCodes.Pop);

                //~~ return;
                
                c.Emit(OpCodes.Ret);
                MonoModHooks.DumpIL(ModContent.GetInstance<BetterNameEditor>(), il);

            }
            catch (Exception e)
            {
                throw new ILPatchFailureException(ModContent.GetInstance<BetterNameEditor>(), il, e);
            }

        }

        private void IL_DrawPlayerName_WhenPlayerIsOffScreen(ILContext il)
        {
            try
            {
                var c = new ILCursor(il);
                var p = typeof(NewMultiplayerClosePlayersOverlay).GetNestedType("PlayerOffScreenCache", BindingFlags.NonPublic);

                c.GotoNext(i => i.MatchLdarg1());

                //~~ namePlatePos = namePlatePos + FontAssets.MouseText.Value.MeasureString(nameToShow) - ChatManager.GetStringSize(FontAssets.MouseText.Value, nameToShow, Vector2.One, -1f) / 2;

                c.Emit(OpCodes.Ldarg_0);

                c.Emit(OpCodes.Ldarg_0);
                c.EmitLdfld(p.GetField("nameToShow", BindingFlags.NonPublic | BindingFlags.Instance));
                c.Emit(OpCodes.Ldarg_0);
                c.EmitLdfld(p.GetField("namePlatePos", BindingFlags.NonPublic | BindingFlags.Instance));
                
                c.EmitDelegate<Func<string, Vector2, Vector2>>((name, pos) => {
                    return pos + Vector2.Divide(FontAssets.MouseText.Value.MeasureString(name) - ChatManager.GetStringSize(FontAssets.MouseText.Value, name, Vector2.One, -1f), 2);
                });
                c.EmitStfld(p.GetField("namePlatePos", BindingFlags.NonPublic | BindingFlags.Instance));
            }
            catch (Exception e)
            {
                throw new ILPatchFailureException(ModContent.GetInstance<BetterNameEditor>(), il, e);
            }
        }
        private void IL_ChatHelper_DisplayMessage(ILContext il)
        {
            var c = new ILCursor(il);
            c.GotoNext(i => i.MatchCall(typeof(NameTagHandler).GetMethod("GenerateTag", BindingFlags.Public | BindingFlags.Static)));
            c.Remove();
            c.GotoNext(i => i.MatchLdstr(" "));
            c.Remove();
            c.Emit(OpCodes.Ldstr, ": ");
        }

        private void IL_UICharacterCreation_Click_Naming(ILContext il)
        {
            var c = new ILCursor(il);
            c.GotoNext(i => i.MatchLdcI4(20));
            c.Remove();
            c.Emit(OpCodes.Ldc_I4, charLimit);
        }

        private void IL_UIWorldCreation_Click_Naming(ILContext il)
        {
            var c = new ILCursor(il);
            c.GotoNext(i => i.MatchLdcI4(27));
            c.Remove();
            c.Emit(OpCodes.Ldc_I4, charLimit);
        }

        private void IL_UIVirtualKeyboard_ctor(ILContext il)
        {
            var c = new ILCursor(il);

            // Move Submit and Cancel buttons and expand layer for them to be selectable, 
            // but only if we edit character or world name 

            // Expand layer
            if (c.TryGotoNext(i => i.MatchLdcR4(266f)))
            {
                c.GotoNext(i => i.MatchLdcR4(266f));
                c.Index++;
                c.Emit(OpCodes.Ldarg_1);
                c.EmitDelegate<Func<float, string, float>>((returnValue, labelText) =>
                {
                    if (labelText == Lang.menu[45].Value || labelText == Lang.menu[48].Value)
                    {
                        return 566f;
                    }
                    else return returnValue;
                });

                c.GotoNext(i => i.MatchLdarg0());
                c.Index += 3;

                // I need outerLayer2 as parent container because it gets disabled if you use a controller
                // c.Emit(OpCodes.Ldarg_0);
                // c.Emit(OpCodes.Ldfld, typeof(UIVirtualKeyboard).GetField("outerLayer2", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance)); // <------  Throws exception 
                // c.Emit(OpCodes.Ldarg_0);
                // c.Emit(OpCodes.Ldarg_1);
                // c.EmitDelegate(CreateNameEditor);

            }
            else Logger.Error("Failed to edit UIVirtualKeyboard");

            // Move Submit button
            if (c.TryGotoNext(i => i.MatchLdcR4(50f)))
            {
                c.Index++;
                c.Emit(OpCodes.Ldarg_1);
                c.EmitDelegate<Func<float, string, float>>((returnValue, labelText) =>
                {
                    if (labelText == Lang.menu[45].Value || labelText == Lang.menu[48].Value)
                    {
                        return 350f;
                    }
                    else return returnValue;
                });
            }
            else Logger.Error("Failed to edit UIVirtualKeyboard");

            // Move Cancel button
            if (c.TryGotoNext(i => i.MatchLdcR4(114f)))
            {
                c.Index++;
                c.Emit(OpCodes.Ldarg_1);
                c.EmitDelegate<Func<float, string, float>>((returnValue, labelText) =>
                {
                    if (labelText == Lang.menu[45].Value || labelText == Lang.menu[48].Value)
                    {
                        return 414f;
                    }
                    else return returnValue;
                });
            }
            else Logger.Error("Failed to edit UIVirtualKeyboard");
        }
        private void On_UIVirtualKeyboard_ctor(On_UIVirtualKeyboard.orig_ctor orig, UIVirtualKeyboard self, string labelText, string startingText, UIVirtualKeyboard.KeyboardSubmitEvent submitAction, Action cancelAction, int inputMode, bool allowEmpty)
        {
            orig(self, labelText, startingText, submitAction, cancelAction, inputMode, allowEmpty);

            if (!(labelText == Lang.menu[45].Value || labelText == Lang.menu[48].Value)) return;

            ((UITextBox)typeof(UIVirtualKeyboard).GetField("_textBox", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(self)).ShowInputTicker = false;

            UIElement outerLayer2 = ((UIElement)typeof(UIVirtualKeyboard).GetField("outerLayer2", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(self));
            outerLayer2.Append(new UINameEditor(outerLayer2, self));
        }
    }
}