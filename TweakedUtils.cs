using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ReLogic.Graphics;
using Terraria.UI.Chat;

namespace BetterNameEditor
{
    public static class TweakedUtils
    {    
        public static void DrawBorderStringFourWay(SpriteBatch sb, DynamicSpriteFont font, string text, float x, float y, Color textColor, Color borderColor, Vector2 origin, float scale = 1f)
        {
            Color color = textColor;
            Vector2 scaleVector = new Vector2(scale, scale);
            Vector2 pos = new Vector2(x, y);

            ChatManager.DrawColorCodedStringWithShadow(sb, font, text, pos, color, 0f, origin, scaleVector);
        }
    }
}